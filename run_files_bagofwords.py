import os
import Config
import bag_of_words
# os.system('python3 naivebayes-V1.py')
accuracy_array = []
for i in range(Config.average_times_file_to_be_run):
    os.system('python3 gen_training_data.py')
    os.system('python3 bag_of_words.py')
    accuracy_array.append(bag_of_words.accuracy())
print(i,') ','Average Accuracy is: ',sum(accuracy_array)/len(accuracy_array),'%')

