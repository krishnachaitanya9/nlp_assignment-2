from sklearn.model_selection import train_test_split
import string
import Config
N    = Config.N
N_80 = Config.N_80
N_20 = Config.N_20
P    = Config.P
P_80 = Config.P_80
P_20 = Config.P_20
test_divide_percentage = Config.test_divide_percentage

def exclude_punctuation(line):
    exclude = set(string.punctuation)
    exclude.remove('-')
    exclude.remove('\'')
    # exclude.remove('.')
    s = ''.join(ch for ch in line if ch not in exclude)
    return s
open(N_80, 'w').close()
open(N_20, 'w').close()
open(P_80, 'w').close()
open(P_20, 'w').close()

file_N = open(N,'r')
file_N_lines = file_N.read().splitlines(False)
file_P = open(P,'r')
file_P_lines = file_P.read().splitlines(False)
N_train, N_test = train_test_split(file_N_lines, test_size=test_divide_percentage)
P_train, P_test = train_test_split(file_P_lines, test_size=test_divide_percentage)
for line in N_train:
    with open(N_80,'a') as filename:
        filename.write(exclude_punctuation(line)+'\n')
for line in N_test:
    with open(N_20,'a') as filename:
        filename.write(exclude_punctuation(line)+'\t'+'N'+'\n')
for line in P_train:
    with open(P_80,'a') as filename:
        filename.write(exclude_punctuation(line)+'\n')
for line in P_test:
    with open(P_20,'a') as filename:
        filename.write(exclude_punctuation(line)+'\t'+'P'+'\n')