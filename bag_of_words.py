import Config
from nltk.corpus import stopwords
from sklearn.model_selection import train_test_split
import math

stopwords_set = set(stopwords.words('english'))
not_stopwords = ["hasn't", "didn't", "don't", "doesn't", "hadn't", "mustn't", "isn't", "wouldn't", "needn't",  "won't", "wasn't", "weren't", "shouldn't", "shan't", "mightn't", "couldn't", "haven't", "aren't", "no", "not"]
for notstopword in not_stopwords:
    stopwords_set.remove(notstopword)

########### Testing Phase ###############
# N_train = Config.N_80
# N_test = Config.N_20
#
# P_train = Config.P_80
# P_test = Config.P_20
############################################

################ Training Phase #############
N_train = Config.N
P_train = Config.P
Final_test_set = Config.final_test_set
#############################################
train_dict = dict()
train_dict['Negative'] = {}
train_dict['Positive'] = {}

no_of_words = dict()

sentence_list = []
negative_set = set()
positive_set = set()
all_words = set()

for each_line in open(N_train, 'r').read().splitlines(False):
    sentence = each_line.split('\t')[1]
    for each_char in sentence.split(' '):
        each_char = each_char.lower()
        if each_char not in stopwords_set:
            negative_set.add(each_char)
            all_words.add(each_char)
            train_dict['Negative'][each_char] = train_dict['Negative'].get(each_char, 0) + 1
            if each_char not in no_of_words:
                no_of_words[each_char] = 1

    sentence_list.append(sentence)

for each_line in open(P_train, 'r').read().splitlines(False):
    sentence = each_line.split('\t')[1]
    for each_char in sentence.split(' '):
        each_char = each_char.lower()
        if each_char not in stopwords_set:
            positive_set.add(each_char)
            all_words.add(each_char)
            train_dict['Positive'][each_char] = train_dict['Positive'].get(each_char, 0) + 1
            if each_char not in no_of_words:
                no_of_words[each_char] = 1
    sentence_list.append(sentence)

shallow_negative_set = negative_set.copy()
shallow_positive_set = positive_set.copy()
negative_set = all_words - shallow_positive_set
positive_set = all_words - shallow_negative_set

negative_lexicon = dict()
positive_lexicon = dict()

for each_line in open(Config.lexicon, 'r').read().splitlines(False):
    word = each_line.split('\t')[0].split('#')[0]
    value = float(each_line.split('\t')[1])
    if value < 0.0:
        negative_lexicon[word] = value
    elif value > 0.0:
        positive_lexicon[word] = value

def combine_probabilities(P,R):
    beta = Config.beta
    return float((((beta*beta) + 1)*P*R)/((beta*beta*P)+R))


def get_probability(char, type_of):
    if char not in stopwords_set:
        if type_of == 'Positive':
            pr_from_training = float(
                (train_dict['Positive'].get(char, 0) + 1) / (len(train_dict['Positive']) + len(no_of_words)))
            if char in positive_set and char in positive_lexicon:
                if positive_lexicon.get(char,0) > 0.5:
                    return 10 * pr_from_training
                elif positive_lexicon.get(char,0) > 0.25:
                    return 5 * pr_from_training
                else:
                    return 3*pr_from_training
                # return 3 * combine_probabilities(pr_from_training, positive_lexicon[char])
            elif positive_lexicon.get(char,0) > 0.2:
                if positive_lexicon.get(char,0) > 0.5:
                    return 10 * pr_from_training
                elif positive_lexicon.get(char,0) > 0.25:
                    return 5 * pr_from_training
                else:
                    return 3*pr_from_training
            else:
                return pr_from_training
        elif type_of == 'Negative':
            pr_from_training = float((train_dict['Negative'].get(char, 0) + 1) / (len(train_dict['Negative']) + len(no_of_words)))
            if char in negative_set or char in negative_lexicon:
                if abs(negative_lexicon.get(char,0)) > 0.5:
                    return 40*pr_from_training
                if abs(negative_lexicon.get(char,0)) > 0.1:
                    return 10*pr_from_training
                else:
                    return 3 * pr_from_training
                # return 3 * combine_probabilities(pr_from_training, -negative_lexicon[char])
            elif abs(negative_lexicon.get(char,0)) > 0.1:
                if abs(negative_lexicon.get(char,0)) > 0.5:
                    return 40*pr_from_training
                else:
                    return 10*pr_from_training

            else:
                return pr_from_training
    else:
        return 0


all_tests_file = Config.test_file
open(all_tests_file, 'w').close()
test_file = open(all_tests_file, 'a')


def test_sentiment(each_line):
    probability_positive = 1.0
    probability_negative = 1.0
    sentence = each_line.split('\t')[1]
    char_set = set()
    previous_char = ''

    for each_char in sentence.split(' '):
        if each_char.lower() not in char_set:
            each_char = each_char.lower()
            # if '.' in each_char:
            #     negative_list.append('.')
            #     each_char = each_char[:-1]
            # if 'n\'t' in each_char or each_char == 'not' or each_char == 'no' or each_char == 'never':
            #     negative_list.append('Neg')
            #     continue
            # elif len(negative_list) >= 1:
            #     if each_char in positive_set and each_char in positive_lexicon:
            #         probability_negative = probability_negative + math.log(1 + get_probability(each_char, 'Positive'))
            #         if probability_negative == 0.0:
            #             break
            #     # probability_positive = probability_positive * get_probability(each_char, 'Positive')
            #     elif each_char in negative_set and each_char in negative_lexicon:
            #         probability_positive = probability_positive + math.log(1 + get_probability(each_char, 'Negative'))
            #         if probability_positive == 0.0:
            #             break
            #     else:
            #         probability_negative = probability_negative + math.log(1 + get_probability(each_char, 'Negative'))
            #         if probability_negative == 0.0:
            #             break
            #         # probability_positive = probability_positive * get_probability(each_char, 'Positive')
            #         probability_positive = probability_positive + math.log(1 + get_probability(each_char, 'Positive'))
            #         if probability_positive == 0.0:
            #             break
            #     if len(negative_list) == 2:
            #         negative_list = list()


            if each_char not in stopwords_set:
                if 'n\'t' in previous_char or previous_char == 'not' or previous_char == 'no' or previous_char == 'never':
                    if each_char in positive_set or each_char in positive_lexicon:
                        probability_negative = probability_negative + math.log(1 + get_probability(each_char, 'Positive'))
                        if probability_negative == 0.0:
                            break
                    elif each_char in negative_set or each_char in negative_lexicon:
                        probability_positive = probability_positive + math.log(1 + get_probability(each_char, 'Negative'))
                        if probability_positive == 0.0:
                            break
                    else:
                        probability_negative = probability_negative + math.log(1 + get_probability(each_char, 'Negative'))
                        if probability_negative == 0.0:
                            break
                        # probability_positive = probability_positive * get_probability(each_char, 'Positive')
                        probability_positive = probability_positive + math.log(1 + get_probability(each_char, 'Positive'))
                        if probability_positive == 0.0:
                            break

                elif 'n\'t' in each_char or each_char == 'not' or each_char == 'no' or each_char == 'never':
                    char_set.add(each_char)
                    previous_char = each_char
                    continue


                else:
                    # probability_negative = probability_negative * get_probability(each_char, 'Negative')
                    probability_negative = probability_negative + math.log(1 + get_probability(each_char, 'Negative'))
                    if probability_negative == 0.0:
                        break
                    # probability_positive = probability_positive * get_probability(each_char, 'Positive')
                    probability_positive = probability_positive + math.log(1 + get_probability(each_char, 'Positive'))
                    if probability_positive == 0.0:
                        break
            char_set.add(each_char)
            previous_char = each_char
    positive = float(probability_positive / (probability_positive + probability_negative))
    negative = float(probability_negative / (probability_positive + probability_negative))

    if positive >= negative:
        test_file.write(each_line + '\t' + 'P' + '\n')
    else:
        test_file.write(each_line + '\t' + 'N' + '\n')

################### Training Phase ####################
# for line in open(N_test, 'r').read().splitlines(False):
#     test_sentiment(line)
#
# for line in open(P_test, 'r').read().splitlines(False):
#     test_sentiment(line)

##########################################################
################### Test Phase ######################

for line in open(Final_test_set, 'r').read().splitlines(False):
    test_sentiment(line)

#####################################################

test_file.close()


###### Accuracy Check ########
def accuracy():
    confusion_file_name = Config.confusion_file
    open(confusion_file_name, 'w').close()
    confusion_file = open(confusion_file_name, 'a')
    test_file = open(all_tests_file, 'r')
    ct = 0
    total = 0
    for each_line in test_file.read().splitlines(False):
        tabs_array = each_line.split('\t')
        if tabs_array[2] == tabs_array[3]:
            ct += 1
        else:
            confusion_file.write(each_line + '\n')
        total += 1
    confusion_file.close()
    print('Accuracy of the Algorithm: ', float(ct / total) * 100, '%')
    return float(ct / total) * 100


# if __name__ == "__main__":
#     accuracy()
