import Config
from nltk.corpus import stopwords
from sklearn.linear_model import LogisticRegression
import numpy as np

stopwords_set = set(stopwords.words('english'))
stopwords_set.remove('not')

N_train = Config.N_80
N_test = Config.N_20

P_train = Config.P_80
P_test = Config.P_20

negative_lexicon = dict()
positive_lexicon = dict()
features_list = []
output_list = []
negative_set = set()
positive_set = set()
all_words = set()

def features_generate(each_line, type_of):
    sentence = each_line.split('\t')[1]
    previous_char = ''
    negative_zero_to_pointone = 0
    negative_pointone_to_pointtwofive = 0
    negative_pointtwofive_pointfive = 0
    negative_pointfive_orgreater = 0

    positive_zero_to_pointone = 0
    positive_pointone_to_pointtwofive = 0
    positive_pointtwofive_pointfive = 0
    positive_pointfive_orgreater = 0
    for each_char in sentence.split(' '):
        each_char = each_char.lower()
        if each_char in positive_lexicon:
            value = positive_lexicon[each_char]
            if 'n\'t' in previous_char or previous_char == 'not' or previous_char == 'no' or previous_char == 'never':
                if value > 0 and value < 0.1 and each_char in positive_set:
                    negative_zero_to_pointone += 1
                elif value >= 0.1 and value < 0.25 and each_char in positive_set:
                    negative_pointone_to_pointtwofive += 1
                elif value >= 0.25 and value < 0.5:
                    negative_pointtwofive_pointfive += 1
                elif value >= 0.5:
                    negative_pointfive_orgreater += 1
            else:
                if value > 0 and value < 0.1 and each_char in positive_set:
                    positive_zero_to_pointone += 1
                elif value >= 0.1 and value < 0.25 and each_char in positive_set:
                    positive_pointone_to_pointtwofive += 1
                elif value >= 0.25 and value < 0.5:
                    positive_pointtwofive_pointfive += 1
                elif value >= 0.5:
                    positive_pointfive_orgreater += 1


        elif each_char in negative_lexicon:
            value = abs(negative_lexicon[each_char])
            if 'n\'t' in previous_char or previous_char == 'not' or previous_char == 'no' or previous_char == 'never':
                if value > 0 and value < 0.1 and each_char in negative_set:
                    positive_zero_to_pointone += 1
                elif value >= 0.1 and value < 0.25 and each_char in negative_set:
                    positive_pointone_to_pointtwofive += 1
                elif value >= 0.25 and value < 0.5:
                    positive_pointtwofive_pointfive += 1
                elif value >= 0.5:
                    positive_pointfive_orgreater += 1


            else:
                if value > 0 and value < 0.1 and each_char in negative_set:
                    negative_zero_to_pointone += 1
                elif value >= 0.1 and value < 0.25 and each_char in negative_set:
                    negative_pointone_to_pointtwofive += 1
                elif value >= 0.25 and value < 0.5:
                    negative_pointtwofive_pointfive += 1
                elif value >= 0.5:
                    negative_pointfive_orgreater += 1
        previous_char = each_char
        features_list.append([negative_zero_to_pointone,
                            negative_pointone_to_pointtwofive+
                            negative_pointtwofive_pointfive+
                            negative_pointfive_orgreater,

                            positive_zero_to_pointone,
                            positive_pointone_to_pointtwofive+
                            positive_pointtwofive_pointfive+
                            positive_pointfive_orgreater])
        if type_of == 'Negative':
            output_list.append(0)
        elif type_of == 'Positive':
            output_list.append(1)

for each_line in open(Config.lexicon, 'r').read().splitlines(False):

    word = each_line.split('\t')[0].split('#')[0]
    if word not in stopwords_set:
        value = float(each_line.split('\t')[1])
        if value < 0.0:
            negative_lexicon[word] = value
        elif value > 0.0:
            positive_lexicon[word] = value

for each_line in open(N_train, 'r').read().splitlines(False):
    sentence = each_line.split('\t')[1]
    for each_char in sentence.split(' '):
        if each_char not in stopwords_set:
            negative_set.add(each_char)
            all_words.add(each_char)
    features_generate(each_line,'Negative')


for each_line in open(P_train, 'r').read().splitlines(False):
    sentence = each_line.split('\t')[1]
    for each_char in sentence.split(' '):
        if each_char not in stopwords_set:
            positive_set.add(each_char)
            all_words.add(each_char)
    features_generate(each_line, 'Positive')

shallow_negative_set = negative_set.copy()
shallow_positive_set = positive_set.copy()
negative_set = all_words - shallow_positive_set
positive_set = all_words - shallow_negative_set

logisticRegr = LogisticRegression(solver = 'liblinear')


logisticRegr.fit(np.array(features_list),np.array(output_list))

test_features_list = []
test_output_list = []

def test_generate(each_line, type_of):

    sentence = each_line.split('\t')[1]
    previous_char = ''
    negative_zero_to_pointone = 0
    negative_pointone_to_pointtwofive = 0
    negative_pointtwofive_pointfive = 0
    negative_pointfive_orgreater = 0

    positive_zero_to_pointone = 0
    positive_pointone_to_pointtwofive = 0
    positive_pointtwofive_pointfive = 0
    positive_pointfive_orgreater = 0
    for each_char in sentence.split(' '):
        each_char = each_char.lower()
        if each_char in positive_lexicon:
            value = positive_lexicon[each_char]
            if 'n\'t' in previous_char or previous_char == 'not' or previous_char == 'no' or previous_char == 'never':
                if value > 0 and value < 0.1 and each_char in positive_set:
                    negative_zero_to_pointone += 1
                elif value >= 0.1 and value < 0.25 and each_char in positive_set:
                    negative_pointone_to_pointtwofive += 1
                elif value >= 0.25 and value < 0.5:
                    negative_pointtwofive_pointfive += 1
                elif value >= 0.5:
                    negative_pointfive_orgreater += 1
            else:
                if value > 0 and value < 0.1 and each_char in positive_set:
                    positive_zero_to_pointone += 1
                elif value >= 0.1 and value < 0.25 and each_char in positive_set:
                    positive_pointone_to_pointtwofive += 1
                elif value >= 0.25 and value < 0.5:
                    positive_pointtwofive_pointfive += 1
                elif value >= 0.5:
                    positive_pointfive_orgreater += 1


        elif each_char in negative_lexicon:
            value = abs(negative_lexicon[each_char])
            if 'n\'t' in previous_char or previous_char == 'not' or previous_char == 'no' or previous_char == 'never':
                if value > 0 and value < 0.1 and each_char in negative_set:
                    positive_zero_to_pointone += 1
                elif value >= 0.1 and value < 0.25 and each_char in negative_set:
                    positive_pointone_to_pointtwofive += 1
                elif value >= 0.25 and value < 0.5:
                    positive_pointtwofive_pointfive += 1
                elif value >= 0.5:
                    positive_pointfive_orgreater += 1


            else:
                if value > 0 and value < 0.1 and each_char in negative_set:
                    negative_zero_to_pointone += 1
                elif value >= 0.1 and value < 0.25 and each_char in negative_set:
                    negative_pointone_to_pointtwofive += 1
                elif value >= 0.25 and value < 0.5:
                    negative_pointtwofive_pointfive += 1
                elif value >= 0.5:
                    negative_pointfive_orgreater += 1
        previous_char = each_char
        test_features_list.append([negative_zero_to_pointone,
                            negative_pointone_to_pointtwofive+
                            negative_pointtwofive_pointfive+
                            negative_pointfive_orgreater,

                            positive_zero_to_pointone,
                            positive_pointone_to_pointtwofive+
                            positive_pointtwofive_pointfive+
                            positive_pointfive_orgreater])
        if type_of == 'Negative':
            test_output_list.append(0)
        elif type_of == 'Positive':
            test_output_list.append(1)

for line in open(N_test, 'r').read().splitlines(False):
    test_generate(line, 'Negative')

for line in open(P_test, 'r').read().splitlines(False):
    test_generate(line, 'Positive')

y_pred = logisticRegr.predict(np.array(test_features_list))
score = logisticRegr.score(np.array(test_features_list), np.array(test_output_list))
print(y_pred)
print(test_output_list)
print(score*100)



