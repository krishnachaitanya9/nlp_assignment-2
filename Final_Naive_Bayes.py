import Config

N_train = Config.N
P_train = Config.P
train_dict = dict()
train_dict['Negative'] = {}
train_dict['Positive'] = {}

no_of_words = dict()

for each_line in open(N_train, 'r').read().splitlines(False):
    sentence = each_line.split('\t')[1]
    for each_char in sentence.split(' '):
        train_dict['Negative'][each_char] = train_dict['Negative'].get(each_char, 0) + 1
        if each_char not in no_of_words:
            no_of_words[each_char] = 1

for each_line in open(P_train, 'r').read().splitlines(False):
    sentence = each_line.split('\t')[1]
    for each_char in sentence.split(' '):
        train_dict['Positive'][each_char] = train_dict['Positive'].get(each_char, 0) + 1
        if each_char not in no_of_words:
            no_of_words[each_char] = 1


def get_probability(char, type):
    if type == 'Positive':
        return float((train_dict['Positive'].get(char, 0) + 1) / (len(train_dict['Positive']) + len(no_of_words)))
    elif type == 'Negative':
        return float((train_dict['Negative'].get(char, 0) + 1) / (len(train_dict['Negative']) + len(no_of_words)))


all_tests_file = Config.final_output_file_for_upload
open(all_tests_file, 'w').close()
test_file = open(all_tests_file,'a')

for each_line in open(Config.final_text_file_for_test,'r').read().splitlines(False):
    sentence = each_line.split('\t')[1]
    probability_positive = 1.0
    probability_negative = 1.0
    sentence_id = each_line.split('\t')[0]
    sentence = each_line.split('\t')[1]
    for each_char in sentence.split(' '):
        probability_negative = probability_negative * get_probability(each_char, 'Negative')
        probability_positive = probability_positive * get_probability(each_char, 'Positive')
    if probability_positive > probability_negative:
        test_file.write(sentence_id+'\t'+'POS'+'\n')
    else:
        test_file.write(sentence_id + '\t' + 'NEG' + '\n')

test_file.close()


