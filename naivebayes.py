import Config
from nltk.corpus import stopwords

stopwords_set = set(stopwords.words('english'))

N_train = Config.N_80
N_test = Config.N_20

P_train = Config.P_80
P_test = Config.P_20

train_dict = dict()
train_dict['Negative'] = {}
train_dict['Positive'] = {}

no_of_words = dict()

for each_line in open(N_train, 'r').read().splitlines(False):
    sentence = each_line.split('\t')[1]
    for each_char in sentence.split(' '):

        train_dict['Negative'][each_char] = train_dict['Negative'].get(each_char, 0) + 1
        if each_char not in no_of_words:
            no_of_words[each_char] = 1

for each_line in open(P_train, 'r').read().splitlines(False):
    sentence = each_line.split('\t')[1]
    for each_char in sentence.split(' '):
        train_dict['Positive'][each_char] = train_dict['Positive'].get(each_char, 0) + 1
        if each_char not in no_of_words:
            no_of_words[each_char] = 1


def get_probability(char, type_of):
    if type_of == 'Positive':
        return float((train_dict['Positive'].get(char, 0) + 1) / (len(train_dict['Positive']) + len(no_of_words)))
    elif type_of == 'Negative':
        return float((train_dict['Negative'].get(char, 0) + 1) / (len(train_dict['Negative']) + len(no_of_words)))


all_tests_file = Config.test_file
open(all_tests_file, 'w').close()
test_file = open(all_tests_file,'a')

for each_line in open(N_test,'r').read().splitlines(False):
    sentence = each_line.split('\t')[1]
    probability_positive = 1.0
    probability_negative = 1.0
    sentence = each_line.split('\t')[1]
    for each_char in sentence.split(' '):
        if each_char not in stopwords_set:
            probability_negative = probability_negative * get_probability(each_char, 'Negative')
            if probability_negative == 0.0:
                break
            probability_positive = probability_positive * get_probability(each_char, 'Positive')
            if probability_positive == 0.0:
                break
    positive = float(probability_positive / (probability_positive + probability_negative))
    negative = float(probability_negative / (probability_positive + probability_negative))
    if positive >= negative:
        test_file.write(each_line + '\t' + 'P' + '\n')
    else:
        test_file.write(each_line + '\t' + 'N' + '\n')

for each_line in open(P_test, 'r').read().splitlines(False):
    sentence = each_line.split('\t')[1]
    probability_positive = 1.0
    probability_negative = 1.0
    sentence = each_line.split('\t')[1]
    for each_char in sentence.split(' '):
        if each_char not in stopwords_set:
            probability_negative = probability_negative * get_probability(each_char, 'Negative')
            if probability_negative == 0.0:
                break
            probability_positive = probability_positive * get_probability(each_char, 'Positive')
            if probability_positive == 0.0:
                break
    positive = float(probability_positive/(probability_positive + probability_negative))
    negative = float(probability_negative/(probability_positive + probability_negative))
    if positive >= negative:
        test_file.write(each_line+'\t'+'P'+'\n')
    else:
        test_file.write(each_line + '\t' + 'N' + '\n')

test_file.close()

###### Accuracy Check ########
def accuracy():
    confusion_file_name = Config.confusion_file
    open(confusion_file_name, 'w').close()
    confusion_file = open(confusion_file_name,'a')
    test_file = open(all_tests_file,'r')
    ct = 0
    total = 0
    for each_line in test_file.read().splitlines(False):
        tabs_array = each_line.split('\t')
        if tabs_array[2] == tabs_array[3]:
            ct += 1
        else:
            confusion_file.write(each_line+'\n')
        total += 1
    print('Accuracy of the Algorithm: ',float(ct/total)*100,'%')
    return float(ct/total)*100








