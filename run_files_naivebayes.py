import os
import Config
import naivebayes
# os.system('python3 naivebayes-V1.py')
accuracy_array = []
for _ in range(Config.average_times_file_to_be_run):
    os.system('python3 gen_training_data.py')
    os.system('python3 naivebayes.py')
    accuracy_array.append(naivebayes.accuracy())
print('Average Accuracy is: ',sum(accuracy_array)/len(accuracy_array),'%')

